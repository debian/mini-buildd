# Generated by Django 4.2.9 on 2024-01-07 14:31

from django.db import migrations

from . import mbd_get_extra_option

SBUILD2INT = {
    "DISABLED": 0,
    "0": 0,
    "IGNORE": 1,
    "1": 1,
    "ERRFAIL": 2,
    "2": 2,
    "WARNFAIL": 3,
    "3": 3,
}


def copy_extra_options(apps, _schema_editor):
    Distribution = apps.get_model("mini_buildd", "Distribution")
    for distribution in Distribution.objects.all():
        distribution.add_depends = mbd_get_extra_option(distribution, "Add-Depends", "")
        distribution.autopkgtest_mode = SBUILD2INT.get(mbd_get_extra_option(distribution, "Autopkgtest-Mode", "0"), 0)
        distribution.deb_build_options = mbd_get_extra_option(distribution, "Deb-Build-Options", "")
        distribution.deb_build_profiles = mbd_get_extra_option(distribution, "Deb-Build-Profiles", "")
        distribution.internal_apt_priority = int(mbd_get_extra_option(distribution, "Internal-APT-Priority", "1"))
        distribution.sbuild_config_blocks = mbd_get_extra_option(distribution, "Sbuild-Config-Blocks", "")
        distribution.sbuild_setup_blocks = mbd_get_extra_option(distribution, "Sbuild-Setup-Blocks", "")
        distribution.save()


class Migration(migrations.Migration):

    dependencies = [
        ('mini_buildd', '0014_add_distribution_extra_options'),
    ]

    operations = [
        migrations.RunPython(copy_extra_options),
    ]

;; Upload Options helper menu in Debian changelog mode
;;
;; This is already helpful, however:
;;
;; * ugly
;; * redundancy fest
;;
;; Elisp cracks please fix and send patch...
;;
(defvar mbd-archives '("test")
	"Identifier strings identifying mini-buildd repositories.\n\nUsed in combination with `mbd-codenames' and `mbd-suites' to form `mbd-distributions'.")
(defvar mbd-codenames '("bullseye" "bookworm" "trixie" "sid")
	"Names identifying Debian base distributions.\n\nUsed in combination with `mbd-archives' and `mbd-suites' to form `mbd-distributions'.")
(defvar mbd-suites '("experimental" "snapshot" "unstable" "hotfix")
	"List of mini-buildd suites suitable to upload to.\n\nUsed in combination with `mbd-archives' and `mbd-codenames' to form `mbd-distributions'.")
(defvar mbd-distributions '()
	"List of \"uploadable\" mini-buildd distributions.\n\nWill be auto-constructed on the basis of `mbd-archives', `mbd-codenames' and `mbd-suites'.\n\nExtensible by setting manually.")

(dolist (id mbd-archives)
	(dolist (codename mbd-codenames)
		(dolist (suite mbd-suites)
			(push (format "%s-%s-%s" codename id suite) mbd-distributions))))

(defun mini-buildd-debian-changelog-mode-hook ()
	"Function called by `debian-changelog-mode-hook' setting up mini-buildd's changelog mode extension."

	(easy-menu-define mini-buildd-menu nil "mini-buildd" '("mini-buildd" :active (not (debian-changelog-finalised-p))))
	(easy-menu-define mini-buildd-ignore-menu nil "mini-buildd ignore" '("Ignore"))
	(easy-menu-define mini-buildd-options-menu nil "mini-buildd options" '("Add Option"))
	(easy-menu-define mini-buildd-distributions-menu nil "mini-buildd distributions" '("Set Distribution"))
	(easy-menu-define mini-buildd-autoports-menu nil "mini-buildd auto-ports" '("Auto-port"))

	(easy-menu-add-item nil '("Changelog") ["--" nil])
	(easy-menu-add-item nil '("Changelog") mini-buildd-menu)
	(easy-menu-add-item nil '("Changelog" "mini-buildd") mini-buildd-distributions-menu)
	(easy-menu-add-item nil '("Changelog" "mini-buildd") mini-buildd-options-menu)
	(easy-menu-add-item nil '("Changelog" "mini-buildd" "Add Option") ["--" nil])
	(easy-menu-add-item nil '("Changelog" "mini-buildd" "Add Option") mini-buildd-autoports-menu)
	(easy-menu-add-item nil '("Changelog" "mini-buildd" "Add Option") mini-buildd-ignore-menu)

	(defun mbd-ignore-lintian ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: lintian-mode=IGNORE"))
	(define-key mini-buildd-ignore-menu [mbd-ignore-lintian]
		'(menu-item "lintian" mbd-ignore-lintian))

	(defun mbd-ignore-autopkgtest ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: autopkgtest-mode=IGNORE"))
	(define-key mini-buildd-ignore-menu [mbd-ignore-autopkgtest]
		'(menu-item "autopkgtest" mbd-ignore-autopkgtest))

	(defun mbd-ignore-piuparts ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: piuparts-mode=IGNORE"))
	(define-key mini-buildd-ignore-menu [mbd-ignore-piuparts]
		'(menu-item "piuparts" mbd-ignore-piuparts))

	(defun mbd-auto-ports ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: auto-ports=<codename>-<id>-<suite>,..."))
	(define-key mini-buildd-options-menu [mbd-auto-ports]
		'(menu-item "auto-ports..." mbd-auto-ports))

	(defun mbd-internal-apt-priority ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: internal-apt-priority=<n>"))
	(define-key mini-buildd-options-menu [mbd-internal-apt-priority]
		'(menu-item "internal-apt-priority..." mbd-internal-apt-priority))

	(defun mbd-deb-build-options ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: deb-build-options[<arch>]=<option> <option>..."))
	(define-key mini-buildd-options-menu [mbd-deb-build-options]
		'(menu-item "deb-build-options..." mbd-deb-build-options))

	(defun mbd-deb-build-profiles ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: deb-build-profiles[<arch>]=<profile> <profile>..."))
	(define-key mini-buildd-options-menu [mbd-deb-build-profiles]
		'(menu-item "deb-build-profiles..." mbd-deb-build-profiles))

	(defun mbd-add-depends ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: add-depends[<arch>]=<dep>, <dep>..."))
	(define-key mini-buildd-options-menu [mbd-add-depends]
		'(menu-item "add-depends..." mbd-add-depends))

	(dolist (dist mbd-distributions)
		(define-key mini-buildd-distributions-menu `[,dist]
			(cons dist `(lambda () (interactive) (debian-changelog-setdistribution ,dist))))
		(define-key mini-buildd-autoports-menu `[,dist]
			(cons dist `(lambda () (interactive) (debian-changelog-add-entry) (insert (format "MINI_BUILDD_OPTION: auto-ports=%s" ,dist)))))))

(add-hook 'debian-changelog-mode-hook 'mini-buildd-debian-changelog-mode-hook)

Consumer's Manual
#################

Introduction
************

This manual should be sufficient for people merely consuming the APT repositories, plus maybe some repository
maintenance like migration -- i.e., everything but actually developing Debian packages.

For example: system administrators, quality assurance personnel.

Default Layout
==============

==================== ========= =================== ========================== ========================= ============================ =======================
  The Default Layout's Suites and Semantics Overview
------------------------------------------------------------------------------------------------------------------------------------------------------------
Suite                Flags     Version restriction Example(``test/bullseye``) Repository                Semantic                     Consumer
==================== ========= =================== ========================== ========================= ============================ =======================
*experimental*       U E 6R    ``~<R><C>+0``       ``~test11+0``              No auto                   *Use at will*                Developer.
snapshot             U E 12R   ``~<R><C>+0``       ``~test11+0``              No auto, but upgrades     *Continuous integration*     Developer, beta tester.
``unstable``         U M 9R    ``~<R><C>+[1-9]``   ``~test11+3``              No auto, but upgrades     *Proposed for live*          Developer, beta tester.
``testing``          M 3R      ``~<R><C>+[1-9]``   ``~test11+2``              No auto, but upgrades     *QA testing*                 Quality Assurance.
``hotfix``           U M 4R    ``~<R><C>+[1-9]``   ``~test11+2+hotfix1``      No auto, but upgrades     *Hotfix proposed for live*   Quality Assurance.
``stable``           6R        ``~<R><C>+[1-9]``   ``~test11+1``              No auto, but upgrades     *Live*                       End customer.
==================== ========= =================== ========================== ========================= ============================ =======================

``U``: Uploadable ``M``: Migrates ``E``: Experimental ``NR``: keeps N Rollback versions ``<R>``: Repository Identity ``<C>``: Codename version.

.. tip:: What's the ``hotfix`` suite good for?

	The ``hotfix`` suite fills the kludge in a situation when a new version is in ``unstable/testing`` (but no yet ready
	for ``stable``), but you need but to do important bug fixes to what is in ``stable`` immediately (it does migrate to
	``stable`` directly).

Release2PinPrio Map
-------------------

============  ====================   ============
NotAutomatic  ButAutomaticUpgrades   APT Pin Prio
============  ====================   ============
no            no                     500
yes           no                     1
no            yes                    n/a
yes           yes                    100
============  ====================   ============

Installation
************

No custom package installations are required; just access the provided service via some web browser, and standard Debian
tools should be enough.

However, it's still *recommended* to install the ``mini-buildd-utils`` package:

.. code:: bash
	 :class: root-code

	 apt install mini-buildd-utils

This toolbox will (among other things) allow you to access the *API* and *events system* from the command line. Also,
this manual might just silently assume it's installed.

Setup
*****

.. tip:: ``firefox``: How can I reset (table) searches (like in webkit browsers)?

	``firefox`` still (2022) does not support this ootb. However, you can enable
	``layout.forms.input-type-search.enabled`` preference as (seemingly still *experimental*) feature via ``about:config``
	in your browser's preferences.

	See: https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Experimental_features#html

APT Bootstrap
=============

To use mini-buildd's repository (or any, really), its APT key must be known/trusted by your system. ``mini-buildd``
conveniently provides that key in a Debian package -- but in that very not-yet-trusted repository.

To "TOFU bootstrap" your system's APT, you may just run:

.. code:: bash
	 :class: root-code

	 mini-buildd-bootstrap-apt <URL>

That tool finds the current codename and the archive ID automatically, and also has some bells and whistles built-in to
work for older releases.

.. tip:: How do I verify mini-buildd's APT key?

	You may compare the key's fingerprint (``apt-key finger``) with mini-buildd's (see :apicall:`pub_key`). There might also be
	other means set up by the local administrator to cross-verify the key.

Alternative: Download archive-keyring.deb and install via dpkg
--------------------------------------------------------------

Find and download the correct deb via :mbdpage:`repositories-dir`, then install it via ``dpkg -i``.

For ``jessie`` or older, you will need an extra ``apt-key add /etc/apt/trusted.gpg.d/mini-buildd-foo.asc`` call.

Will yield the same end result (i.e., keyring package installed) as the recommended method.

Alternative: Doing 'insecure' apt update and install
----------------------------------------------------

Let's say you have your sources list configured manually, but mini-buildd's key is not yet trusted.

For anything ``stretch`` or newer, you can run:

.. code:: bash
	 :class: root-code

	 apt --allow-insecure-repositories update
	 apt --allow-unauthenticated install <ARCHIVE>-archive-keyring
	 apt update

For ``jessie`` or older, this should still work just omitting any ``--allow*`` options apt may not yet know about.

Will yield the same end result (i.e., keyring package installed) as the recommended method.

Alternative: Manually install the APT key file
----------------------------------------------

Download the key via :apicall:`pub_key`, then install it under ``/etc/apt/trusted.gpg.d/foo.asc``. For example:

.. code:: bash
	 :class: root-code

	 mini-buildd-api pub_key http://foo.bar.org:8066 >/etc/apt/trusted.gpg.d/foo.asc
	 # OR
	 wget http://foo.bar.org:8066/mini_buildd/api/pub_key/?output=plain --output-document /etc/apt/trusted.gpg.d/foo.asc

For ``jessie`` or older, you will need an extra ``apt-key add foo.asc`` call.

Different end result than the recommended method -- you now have an extra local configuration file to maintain.

Sources Lists
=============

You can retrieve any apt line(s) via the :apicall:`sources_list`.

Here is an example on how to do it on the shell (requires :debpkg:`mini-buildd-utils`):

.. code:: bash

	 mini-buildd-api sources_list <ENDPOINT> --codenames sid --suites stable

Just add lines to ``/etc/apt/sources.list`` or individual files in ``/etc/apt/sources.list.d/``.

Symlinks to keyring package
---------------------------

The resp. archive's *keyring package* also provides a library of predefined sources list files you can just symlink to:

.. code:: bash
	 :class: root-code

	 cd /etc/apt/sources.list.d
	 ln -s /usr/share/<ARCHIVE>-archive-keyring/sources.list.d/<CODENAME>/foo.list .
	 apt-get update

User
====

To be able to **use advanced functionality** (for example, create package subscriptions, access restricted API calls, or
upload your GnuPG public key, migrate packages), create a *user account*:

#. `Register a user account </accounts/register/>`_.
#. `Setup your profile </accounts/profile/>`_ (package subscriptions, GnuPG key upload).

Aptitude
========

In case you use *aptitude*: By default, it does not show distribution or origin of packages -- but this might be very
helpful.

To show the **distribution** of packages, just add ``%t`` to the package display format (:debbug:`484011`). For example,
I do prefer this setting for the *Package-Display-Format*::

	aptitude::UI::Package-Display-Format "%c%a%M%S %p %t %i %Z %v# %V#";

The origin cannot be shown in the package display format (:debbug:`248561`). However, you may change the grouping to
categorize with "origin". For example, I do prefer this setting for the *Default-Grouping*::

	aptitude::UI::Default-Grouping "task,status,pattern(~S~i~O, ?true ||),pattern(~S~i~A, ?true ||),section(subdirs,passthrough),section(topdir)";

This will group installed packages into an *Origin->Archive* hierarchy.

Additionally to aptitude's default "Obsolete and locally installed" top level category (which only shows packages not in
any apt archive), this grouping also more conveniently shows installed package _versions_ which are not currently in any
repository (check "Installed Packages/now").

Workflows
*********

.. tip:: How can I upgrade after Debian main release version fix?

	Since Debian ``wheezy``, Debian uses only one number as main release version. I.e., squeeze main version was ``6.0``,
	while wheezy's main version is just ``7``.

	(Assuming repo ``test`` for the following, replace as needed.)

	So, the new default version appendix in mini-buildd is now just something like ``~test7`` (wheezy) or ``~test11``
	(bullseye), analogous to what Debian does with ``bpo``.

	On a production system, the admin may/should decide at some point (i.e., when a new codename is introduced) to switch
	to the new scheme.

	Let's say the admin decided to do the change with the introduction of buster. Then ``~test90`` (stretch, oldstyle)
	would be bigger than ``~test10`` (buster, new style). To be able to upgrade the packages, use an apt preference like
	this:

	.. code-block::
		:caption: /etc/apt/preferences.d/mbd-buster-upgrade.pref

			Package: *
			Pin: release a=buster-test-*
			Pin-Priority: 1001

API and Events
==============

Documentation of all ``API`` calls is in-code only; on mini-buildd's web interface, you find a summary on :mbdpage:`api`
(there is also a convenience menu in this manual in the left sidebar).

On the web interface, API calls are embedded where appropriate; events are shown on :mbdpage:`events`.

Access via Shell
----------------

Access from the shell via :mbdcommand:`mini-buildd-api` or :mbdcommand:`mini-buildd-events`, respectively.

To get started, try:

.. code:: bash

	 mini-buildd-api --help
	 mini-buildd-events --help

.. tip:: How to save credentials when working from the shell?

	Credentials are handled via :debpkg:`python3-keyring`, which *should just work* -- i.e., choose a working && secure
	backend for you automatically.

	If you want to do unnattended authorized task, see this example (forcing to save passwords as **plain text file**):

	.. literalinclude:: ../examples/mini-buildd-utils/keyringrc.cfg
	 :caption: /usr/share/doc/mini-buildd-utils/examples/keyringrc.cfg

	.. seealso:: py3-keyring bugs that may catch you as well: `391 <https://github.com/jaraco/keyring/issues/391>`_, `514 <https://github.com/jaraco/keyring/issues/514>`_

Access via Python
-----------------

Python documentation is `here <code/mini_buildd/mini_buildd.client.html>`_ (or maybe that is still just where it *should
be eventually* ;). At least you get the definitions, and you still have the source...)

To get started, look at:

.. literalinclude:: ../examples/mini-buildd-utils/python-client
	 :caption: /usr/share/doc/mini-buildd-utils/examples/python-client

``python`` code might be more suitable for elaborated scripts; for simple tasks, rather use
:mbdcommand:`mini-buildd-api`.

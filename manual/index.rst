mini-buildd
###########

A custom build daemon for *Debian*-based distributions with :ref:`all batteries included <abstract:Features>`.

.. toctree::
	 :maxdepth: 2

	 abstract
	 roadmap
	 consumer
	 developer
	 administrator
	 auto_admonitions
	 Python <python/modules>
